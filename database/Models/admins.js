const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class Admins extends Model {

    constructor () {
        super()
        this.tableName = 'admins';
        this.sqlSetup = './database/Models/sql/create-admins.sql';
    }

}

module.exports = new Admins();


