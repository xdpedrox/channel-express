const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class Channel extends Model {

    constructor () {
        super()
        this.tableName = 'channels';
        this.sqlSetup = './database/Models/sql/create-channels.sql';
    }

}

module.exports = new Channel();


