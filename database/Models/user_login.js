const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class UserLogin extends Model {

    constructor() {
        super()
        this.tableName = 'users_login';
        this.sqlSetup = './database/Models/sql/create-users_login.sql';
    }

    async getByPlatformId (platform, platformId) {
        let conn;
        try {
            conn = await pool.getConnection();
            //     console.log(conn)
            const sql = `SELECT * FROM ${this.tableName} WHERE platform_id = ? AND platform = ? LIMIT 1;`;
            const userLogin = await conn.query(sql, [platformId, platform]);
            console.log(userLogin);
            console.log(`${this.tableName} getOne`);

            if (userLogin.length == 0) {
                return null;
            }
            return userLogin[0];
        } catch (err) {
            console.log(err)
            throw err;
        } finally {
            if (conn) return conn.end();
        }
    }

}

module.exports = new UserLogin();


