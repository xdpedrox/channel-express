const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class Users extends Model {

    constructor () {
        super()
        this.tableName = 'users';
        this.sqlSetup = './database/Models/sql/create-users.sql';
    }
}

module.exports =  new Users();


