const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class ChannelGroups extends Model {

    constructor () {
        super()
        this.tableName = 'channel_groups';
        this.sqlSetup = './database/Models/sql/create-channel_groups.sql';
    }



}

module.exports = new ChannelGroups();


