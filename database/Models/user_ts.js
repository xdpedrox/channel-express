const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class UsersTs extends Model {

    constructor (name) {
        super()
        this.tableName = 'users_ts';
        this.sqlSetup = './database/Models/sql/create-users_ts.sql';
    }

}

module.exports = new UsersTs();


