const pool = require('../../mariadb');
const mariadb = require('mariadb');

var fs = require('fs');

module.exports = class Model {


  async setup() {
    // function setup() {
    let conn;
    try {
      const createUser = fs.readFileSync(this.sqlSetup).toString();

      conn = await pool.getConnection(this.sqlSetup);

      conn.query(createUser)
        .then(data => {
          // console.log(data)
          console.log(`Created db: ${this.tableName}`)

        }).catch(err => {
          if (err.code == 'ER_TABLE_EXISTS_ERROR') {
            console.log(`${this.tableName} table already exists`)
            return
          }
          console.log(err)
        })
    } catch (err) {
      console.log(err)
      throw err;

    } finally {
      if (conn) return conn.end();
    }
  }

  add(params) {

    return pool.getConnection()
      .then(conn => {
        //     console.log(conn)
        let sqlupdate = ''

        // const keys =
        const values = Object.values(params)
        const keys = Object.keys(params).join(', ')

        let numberValues = [];

        for (let index = 0; index < values.length; index++) {
          numberValues.push('?');
        }

        const sql = `INSERT INTO ${this.tableName} (${keys}) VALUES (${numberValues.join(', ')});`
        //             console.log(sql)
                    console.log(values)
        console.log(`Insert ${this.tableName}`)

        return conn.query(sql, values)
      })
      .catch(err => {
        if (err.code == 'ER_NO_DEFAULT_FOR_FIELD') {
          console.log(`Parameter missing DEFAULT_FOR_FIELD - ${this.tableName} create`)
          return
        } else if (err.code == 'ER_MISSING_PARAMETER') {
          console.log(`Missing parameter - ${this.tableName} create`)
          return
        } else if (err.code == 'ER_DUP_ENTRY') {
          console.log(`Duplicate Entry - insert ${this.tableName}`)
          return
        }
      })

    //             console.log(valuedReturned)
    //             return valuedReturned;


    if (conn) return conn.end();
    //             console.log(err)



  }


  async set(id, params) {
    let conn;
    try {
      //     console.log(conn)
      let sqlupdate = ''
      for (let [key, value] of Object.entries(params)) {
        console.log(`${key}: ${value}`);
        sqlupdate = sqlupdate + `${key} = ? `;
      }
      const sqlParam = Object.values(params)
      sqlParam.push(id)
      conn = await pool.getConnection();

      const sql = `UPDATE ${this.tableName} SET ${sqlupdate}WHERE id = ?;`
      const logs = await conn.query(sql, sqlParam)
      console.log(`${this.tableName} updated`)
    } catch (err) {
      console.log(err)
      throw err;
    } finally {
      if (conn) return conn.end();
    }

  }
  async remove(id) {
    let conn;
    try {
      conn = await pool.getConnection();
      //     console.log(conn)
      const sql = `DELETE FROM ${this.tableName} WHERE id = ?`;
      await conn.query(sql, [id])
      // console.log(users)
      console.log(`${this.tableName} removed`)
    } catch (err) {
      console.log(err)
      throw err;
    } finally {
      if (conn) return conn.end();
    }
  }

  async get() {
    let conn;
    try {
      conn = await pool.getConnection();
      //     console.log(conn)
      const sql = `SELECT * FROM ${this.tableName};`;
      const user = await conn.query(sql);
      console.log(user);
      console.log(`${this.tableName} get`);
      return user;
    } catch (err) {
      console.log(err)
      throw err;
    } finally {
      if (conn) return conn.end();
    }
  }

  async getOne(id) {
    let conn;
    try {
      conn = await pool.getConnection();
      //     console.log(conn)
      const sql = `SELECT * FROM ${this.tableName} WHERE platform = ? LIMIT 1;`;
      const user = await conn.query(sql, [id]);
      console.log(user);
      console.log(`${this.tableName} getOne`);
      return user[0];
    } catch (err) {
      console.log(err)
      throw err;
    } finally {
      if (conn) return conn.end();
    }
  }

}
