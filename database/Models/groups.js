const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class Groups extends Model {

    constructor() {
        super()
        this.tableName = 'groups';
        this.sqlSetup = './database/Models/sql/create-groups.sql';
    }

}



module.exports = new Groups();


