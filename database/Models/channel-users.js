const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class ChannelUsers extends Model {

    constructor () {
        super()
        this.tableName = 'channel_users';
        this.sqlSetup = './database/Models/sql/create-channel_users.sql';
    }

}

module.exports = new ChannelUsers();


