const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class Zones extends Model {

    constructor() {
        super()
        this.tableName = 'zones';
        this.sqlSetup = './database/Models/sql/create-zones.sql';
    }

}

module.exports = new Zones();


