const pool = require('../mariadb');
const Model = require('./lib/mariadb-model');
const mariadb = require('mariadb');


class AdminRank extends Model {

    constructor () {
        super()
        this.tableName = 'admin_ranks';
        this.sqlSetup = './database/Models/sql/create-admin_ranks.sql'
    }

}

module.exports = new AdminRank();

