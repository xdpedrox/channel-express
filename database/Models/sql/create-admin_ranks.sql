create table admin_ranks
(
    id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    admin_level int NOT NULL,

    PRIMARY KEY(id)
);
