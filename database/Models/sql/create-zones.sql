create table zones
(
    id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    nextChannelNumber int NOT NULL,
    nextSpacerNumber  int NOT NULL,
    lastChannelId     int NOT NULL,
    PRIMARY KEY(id)

);
