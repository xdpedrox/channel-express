create table channel_groups
(
    id                int auto_increment,
    name              varchar(255) null,
    level             int(10) DEFAULT 1,
    createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY(id)
);
