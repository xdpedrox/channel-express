CREATE TABLE users_login
(
    id INT AUTO_INCREMENT,
    platform VARCHAR(255) NOT NULL,
    platform_id INT NOT NULL,
    user_id int NOT NULL,
    createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    UNIQUE (platform_id, platform)

)
