create table channels
(
    id                  int auto_increment,
    status              varchar(255) DEFAULT 'good',
    name                varchar(255) NOT NULL,
    channel_order       int NOT NULL,
    spacer_number       int NOT NULL,
    empty_spacer_cid    int NOT NULL,
    spacer_bar_cid      int NOT NULL,
    group_id            int NULL,
    last_used           DATETIME DEFAULT CURRENT_TIMESTAMP,
    zone_id             int          NOT NULL,
    createdAt           DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatedAt           DATETIME ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY(id),
    CONSTRAINT status_contraint
    CHECK
        (
        STATUS
            = 'good' OR
        STATUS
            = 'removed' OR
        STATUS
            = 'unused'
    )
);
