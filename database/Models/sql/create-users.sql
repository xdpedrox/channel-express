CREATE TABLE users
(
    id INT AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    status VARCHAR (255) NOT NULL,
    nickname VARCHAR(20) NOT NULL,
    createdAt DATETIME DEFAULT CURRENT_TIMESTAMP,
    updatedAt DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id)
);
