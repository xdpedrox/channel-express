const AdminRanks = require('../database/Models/admin_ranks')
const Admins = require('../database/Models/admins')
const Channel = require('../database/Models/channel')
const ChannelGroups = require('../database/Models/channel_groups')
const ChannelUsers = require('../database/Models/channel-users')
const Groups = require('../database/Models/groups')
const Users = require('../database/Models/users')
const UserLogin = require('../database/Models/user_login')
const UserTs = require('../database/Models/user_ts')
const Zones = require('../database/Models/zones')




AdminRanks.setup();
Admins.setup();
Channel.setup();
ChannelGroups.setup();
ChannelUsers.setup();
Groups.setup();
Users.setup();
UserLogin.setup();
UserTs.setup();
Zones.setup();

Zones.add({id: 1, name: 'Premium', nextChannelNumber: '1', nextSpacerNumber: '10', lastChannelId: '3'});
Zones.add({id: 2, name: 'Member', nextChannelNumber: '1', nextSpacerNumber: '10', lastChannelId: '3'});
Zones.add({id: 3, name: 'Free', nextChannelNumber: '1', nextSpacerNumber: '10', lastChannelId: '3'});

AdminRanks.add({id: 1, name: 'Admin', admin_level: 1})

ChannelGroups.add({id: 1, name: 'Channel Owner', level: 4})
ChannelGroups.add({id: 2, name: 'Channel Admin', level: 3})
ChannelGroups.add({id: 3, name: 'Channel Mod', level: 2})
ChannelGroups.add({id: 4, name: 'Membro', level: 1})

// Channel.add({
//     name: 'Member',
//     channel_order: 1,
//     status: 'good',
//     spacer_number: 1,
//     empty_spacer_cid: 1,
//     spacer_bar_cid: 1,
//     group_id: 1,
//     zone_id: 1,
// })


// UserLogin.add({
//     // id: 1,
//     type: 'steam',
//     user_id: 1
// })
