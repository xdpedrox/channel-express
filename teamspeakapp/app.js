/**
 * Basic example demonstrating passport-steam usage within Express framework
 */
var express = require('express')
    , util = require('util')
    var bodyParser = require('body-parser')
    //Routes Import
const channelRoutes = require('./routes/ChannelRoutes');
// const channelGroupRoutes = require('./routes/ChannelGroupRoutes');
// const serverGroupRoutes = require('./routes/ServerGroupRoutes');
// const userRoutes = require('./routes/UserRoutes');
// const serverRoutes = require('./routes/ServerRoutes');


var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Default Routes
app.use('/channel', channelRoutes);
// app.use('/channel_group', channelGroupRoutes);
// app.use('/server_group', serverGroupRoutes);
// app.use('/user', userRoutes);
// app.use('/server', serverRoutes);


//Error handling
app.use((error, req, res, next) => {

    //Logging
      // let log = new Log(error);
  
      // log.save().then((log) => {
      // console.log('Log Saved to the Database Saved')
      // })
      // console.log(500);
  
    res.status(500).json({})
    // .json({
    //   // status: error.status,
    //   // message: error.message,
    //   data: error.error
    // });
  });
  



app.listen(1337);


