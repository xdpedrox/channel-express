
const {ObjectId} = require('mongodb');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');
// const reply = require('../apiReply');

//Modules
const ts = require('../teamspeak/index')


exports.index = (req, res, next) => {

    const data = {
        message: "Working"
    }

    res.status(200).json(data);

}

exports.create = (req, res, next) => {

    //   {
    //     clid: 1,
    //     move: true,
    //     area: {
    //       name: 'pedro',
    //       nextSpacerNumber: 1,
    //       nextChannelNumber: 1,
    //       lastChannelId: 1
    //     },
    //     channel: {
    //       name: 'pedro',
    //       password: '123',
    //       topic: 'topic',
    //       description: 'description'
    //     }
    //   }

    const params = req.body;

    ts.Channel.createChannels(
        params.areaName, 
        params.areaNextSpacerNumber, 
        params.areaNextChannelNumber, 
        params.areaLastChannelId, 
        params.channelname, 
        params.channelpassword, 
        params.channelTopic, 
        params.channelDescription, 
        params.clid, params.move
        )
    .then(data => {
        console.log(data)
        res.status(200).json(data);
    })
    .catch(err => {
        if (err.id == 771) {
            console.log("Channel Already axists")
            res.status(409).json({message: "Channel already in use"});
        } else {
            next('failed');

        }
    })
}
