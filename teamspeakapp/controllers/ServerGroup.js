
const {ObjectId} = require('mongodb');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');
const reply = require('../apiReply');

//Modules
const teamSpeak = require('../teamSpeak/library/app')


exports.getTeamConfig = (req, res, next) => {

  //Input Parameters of the Route
  req.input = {...req.input,
      teamId: req.params.teamId,
    }

  //Specify which databases to load
  const loadDb = ['team'];
  req.loadDb = loadDb;
  next();
}

