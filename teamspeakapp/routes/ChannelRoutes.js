const express = require('express');
const { body } = require('express-validator/check');


//Verifications
// const {isAuth} = require('../../middleware/auth/isAuth')


//Controllers
const channelController = require('../controllers/Channel');

const router = express.Router();



// //Get Teams
router.post('/create', channelController.create);

// //Get Teams
router.get('/index', channelController.index);


module.exports = router;