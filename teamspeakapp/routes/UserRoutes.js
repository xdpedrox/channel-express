const express = require('express');
const { body } = require('express-validator/check');


//Verifications
// const {isAuth} = require('../../middleware/auth/isAuth')


//Controllers
const userController = require('../controllers/User');

const router = express.Router();



// //Get Teams
// router.get('/team', teamController.getTeams);

// //Get Team
// router.get('/team/:teamId', teamController.getTeamConfig, db.loadConfig, teamController.getTeam);

// // //Create Team
// router.put('/team', isAuth, teamController.createTeamConfig, db.loadConfig, isPartOfAnotherTeam, hasSpentTimeOnServer, isOnline, teamController.createTeam);


// //Change Team Name
// router.patch('/team/:teamId', isAuth, teamController.changeTeamNameConfig, db.loadConfig, isChannelAdmin, teamController.changeTeamName);

// //Upload Logo
// router.post('/logo/:teamId', dbLoad, isAuth, isChannelAdmin, teamController.uploadLogo);

// //Delete logo
// router.delete('/logo/:teamId', dbLoad, isAuth, isChannelAdmin, teamController.deleteLogo);

// // change user permissions to team
// router.post('/member/:teamId', isAuth, teamController.setUserPermissionsConfig, db.loadConfig, isChannelMod, isOnline, teamController.setUserPermissions);

// //Move Team
// router.patch('/move/:teamId',  isAuth, teamController.moveChannelConfig, db.loadConfig, isChannelAdmin, isOnline, teamController.moveChannel);

// //Create Server Group
// router.patch('/servergroup/:teamId',  isAuth, teamController.changeTeamNameConfig, db.loadConfig, isChannelAdmin, teamController.createServerGroup);

module.exports = router;