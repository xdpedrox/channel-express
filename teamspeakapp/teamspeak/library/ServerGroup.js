const ts3 = require('../tsConnect');
const Core = require('./Core');
const ChannelGroup = require('./ChannelGroup')
const Channel = require('./Channel')
const User = require('./User')

/**
 * Create Server Group to be used for the Server Icons
 */
const create = (name) => {
  //Make a Copy of the template ServerGroup
  return ts3.serverGroupCopy(config.groupSettings.serverGroupTemplate, 0, 1, team.teamName)
  .catch(err =>{
    throw err
  })
}



/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const add = (uuid, sgid) => {

  //Get users.js DBID
  return TsServer.getCldbidFromUid(uuid)
    .then(cldbid => {
      //Get list of SubChannels
      return ts3.serverGroupAddClient(cldbid, sgid)
        .then((data) => {
          return data;
        })
        .catch(err => {
          throw err;
          // throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
        })

    })
    .catch(err => {
      throw err;
      // throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
    })
}


/**
* Removes client from ServerGroup
* @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
* @param {Number} sgid - ServerGroup ID
*/
const remove = (uuid, sgid) => {

  //Get users.js DBID
  return TsServer.getCldbidFromUid(uuid)
    .then(cldbid => {
      //Remove Server Group
      return ts3.serverGroupDelClient(cldbid, sgid)
        .then((data) => {
          return ApiReply('removeUserByUuid: Success', data)
        })
    })
    .catch(err => {
      throw failedApiReply(err, 'removeUserByUuid: Failed to get cldbid');
    })
}

const channelView = (cid) => {
  return serverView()
      .then(view => {
          // console.log(view);
          return view.filter(channel => channel.pid == cid);
      }).catch(err => {
          throw failedApiReply(err, 'API channelView: Failed to get channels');
      })
}

module.exports = {
  create,
  add,
  remove,
}
