const responseError = (code, msg) => {
    return {
        code: code,
        msg: msg
    }
}