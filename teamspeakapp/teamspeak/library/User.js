const ts3 = require('../tsConnect');
const Core = require('./Core');
const ChannelGroup = require('./ChannelGroup')
const ServerGroup = require('./ServerGroup')
const Channel = require('./Channel')

const _ = require('lodash')
// const generateUserDescription()

/**
 * Uses the IP to check if user is connected to the teamspeak server
 * @param {String} ip
 */
const getUserByIP = (ip) => {

  // Get client list with the filtered ip and type
  return ts3.clientList({
      connection_client_ip: ip,
      client_type: 0,
    })
    .then(clients => {
      return clients

    }).catch(err => {
      console.log(err)
      throw err

    })
}

/**
 * Moves client to the first channel of a team.
 * @param {Number} mainChannelId - CID of the Main Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 */
const moveToFirstSubChannel = (mainChannelId, clid) => {

  return Channel.subChannelList(mainChannelId)
    .then(channels => {
      //Set Move Client
      return ts3.clientMove(clid, channels[0]._propcache.cid)
        .catch(err => {
          if (err.id == 770) {
            return "users.js is already in the channel"
          } else {
            throw err;
          }
          //   failedApiReply(err, 'moveToFirstChannel: Failed to Move Client');
        })
    })
    .catch(err => {
      // console.log('lol')
      throw err;
      // failedApiReply(err, 'moveToFirstChannel: Failed to get subChannelList');
    })
}


const changeUserDescription = (uuid, description) => {



  if ((_.isString(uuid)) && (_.isString(description))) {



      return TsServer.getClientByUID(uuid)
      .then(client => {

          let params = {
              clid: client.getID(),
              client_description: description
          };


          return ts3.execute("clientedit", params)
          .then(data => {
              return true;
          })
          .catch(err => {
              throw err;
              // failedApiReply('changeUserDescription: Error changin user description', err)
          })

      })
      .catch(err => {
          throw err;
          failedApiReply('changeUserDescription: Error fetching client', err)
      })



  } else {
      throw err;
      // failedApiReply('changeUserDescription: Invalid Input', 500)
  }
};

module.exports = {
  getUserByIP,
  moveToFirstSubChannel,
  changeUserDescription
}
