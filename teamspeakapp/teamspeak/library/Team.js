const ts3 = require('../tsConnect');
const ServerGroup = require('./ServerGroup')
const ChannelGroup = require('./ChannelGroup')
const User = require('./User')
const Channel = require('./Channel')
const TsServer = require('./TsServer')




/**
 * Create New Team by Creating Channels of Claming
 * @param {String} name
 * @param {String} password
 * @param {String} ownerUuid
 * @param {Number} areaId
 * @param {boolean} move
 */
const create = (name, password, topic, description, nextSpacerNumber, lastChannelId, uuid, clid, move) => {

	//Create Channel
	return createChannels (name, password, topic, description, nextSpacerNumber, lastChannelId, clid, move)
		.then((channels) => {
			//Add user to the Channel and Give Permissions
			return addUserToTeam(channels.mainChannelId, null, uuid, channelGroupId)
				.then(updatedTeam => {
					return ApiReply('createChannels: Channels Sucessfully Created', updatedTeam)


				})
				.catch(err => {
					throw failedApiReply(err, 'createTeam: Failed to add to Team');
				})
		})
		.catch(err => {
			throw failedApiReply(err, 'createTeam: Failed to create Channels');
		})

}



/**
 * Adds users.js to the Team (inc: ServerGroup, ChannelAdmin, DB...)
 * @param {string} teamObjectID - Team objectID from the Database
 * @param {String} memberObjectID - Member objectID from the Database
 * @param {Number} permission - 1 to 4, 1 = Founder, 4 = Member
 */
const addUser = (mainChannelId, serverGroupId, uuid, channelGroupId) => {

	// Add to serverGroup
	if (serverGroupId != null) {
		ServerGroup.add(uuid, serverGroupId)
			.catch(err => {
				throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
			})
	}
	//If team has a servergroup, then add the user to it

	// Add channelUser
	return ChannelGroup.set(channelGroupId, mainChannelId, uuid)
		.then(() => {
			return newTeam;
		})
		.catch(err => {
			throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
		})

}


/**
 * Removes user from the team. (inc: ServerGroup, ChannelAdmin, DB...)
 * @param {String} teamObjectID - Team objectID of the Database
 * @param {String} memberObjectID - Member objectID of the Database
 */
const removeUser = (mainChannelId, serverGroupId, uuid) => {


	// Add to serverGroup
	if (serverGroupId != null) {
		ServerGroup.remove(uuid, serverGroupId)
			.catch(err => {
				throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
			})
	}
	//If team has a servergroup, then add the user to it

	// Add channelUser
	return ChannelGroup.set(config.groupSettings.guestChannelGroup, mainChannelId, uuid)
		.then(() => {
			return newTeam;
		})
		.catch(err => {
			throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
		})
}

/**
 * Creates a Channel a Channel - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Id
 * @param {Number} areaId - Id of the Area where the channel is going to be created.
 * @param {boolean} move - Move the Client to the channel
 */
const createChannels = (channelName, password, topic, description, nextSpacerNumber, lastChannelId, clid, move) => {

	//Create Channels
	return Channel.createPack(channelName, password, topic, description, nextSpacerNumber, lastChannelId)
		.then(channelIds => {
			console.log(channelIds)
			if (move) {
				User.moveToFirstSubChannel(channelIds.mainChannelId, clid)
					.catch(err => {
						console.log(err)
					})
			}
			return channelIds

		}).catch(err => {
			throw err
			// failedApiReply(err, 'createChannels: Failed to create Group of Channels');
		})

}


/**
 * Creates a ServerGroup for the team.
 * @param {String} uuid - TeamSpeak Unique ID
 */
const createServerGroup = (name, teamMembers) => {


	//Make a Copy of the template ServerGroup
	return ts3.serverGroupCopy(config.groupSettings.serverGroupTemplate, 0, 1, name)
		.then(group => {


			//Works Like a ForEach Loop but it's async
			let addMembersToGroupArray = teamMembers.map(teamMember => {


				//get cldbid of the user
				return TsServer.getCldbidFromUid(teamMember.memberUuid)
					.then(cldbid => {

						//Add ServerGroup to Client
						return ts3.serverGroupAddClient(cldbid, group.sgid)
							.then(() => {

								return group.sgid

							})
							.catch(err => {
								throw failedApiReply(err, 'createServerGroup: Error Adding user to group');
							})

					})
					.catch(err => {
						throw failedApiReply(err, 'createServerGroup: Error fetching cldbid ');
					})

			});

			return Promise.all(addMembersToGroupArray)
				.then(array => {
					return array;
				})
				.catch(err => {
					throw failedApiReply(err, 'channelRemoveChannelGroupClients: Error Setting ChannelGroup.');
				})


		})
		.catch(err => {
			throw failedApiReply(err, 'createServerGroup: Error Copying Group');
		})

}



const resetChannelPermissions = (mainChannelId, uuid, permission, serverGroupId) => {

	if (permission == 1) {
		channelGroupId = config.groupSettings.channelAdmin;

	} else if (permission == 2) {
		channelGroupId = config.groupSettings.channelAdmin;

	} else if (permission == 3) {
		channelGroupId = config.groupSettings.channelMod;

	} else if (permission == 4) {
		channelGroupId = config.groupSettings.channelMember;

	} else {
		return 5;

	}

	//Set Channel Groups
	return ChannelGroup.set(channelGroupId, mainChannelId, uuid)
		.then(data => {
			console.log(data)

			//If team has a servergroup, then add the user to it
			if (serverGroupId != null) {
				ServerGroup.add(uuid, serverGroupId)
					.then(info => {
						console.log(info)
					})
					.catch(err => {
						if (err.id == 2561) {
							return 'users.js Already bellongs to this server group'
						}
						throw err
						'addUserToTeam: setClientServerGroupByUid failed';
						// throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
					})
			}
			return data;
		})
		.catch(err => {
			throw err
			'addUserToTeam: Failed updating the database';
			// throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
		})
}


/**
 * Swaps channel position with another channel.
 * @param {string} teamObjectID - Team objectID from the Database
 * @param {String} memberObjectID - Member objectID from the Database
 */
const moveChannel = (name, mainChannelId, uuid, teamMembers) => {

	//Claim Channels on TeamSpeak
	return claimChannels(name, '', uuid, channel._id, true)
		.then(() => {

			//Free Up Old Channel
			return freeUpChanels()
				.then(() => {

					//Giving Channel Permission to Users
					return ChannelGroup.reassign(mainChannelId, teamMembers) 
						.then(info => {
							//Return Success!
							return ApiReply('Channel Group Assigned', info)
						})
						.catch(err => {
							throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
						})

				})
				.catch(err => {
					throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
				})

		})
		.catch(err => {
			throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
		})
}


module.exports = {
	create,
	addUser,
	removeUser,
	// moveChannel,
	createChannels,
	createServerGroup,
	resetChannelPermissions,
}
