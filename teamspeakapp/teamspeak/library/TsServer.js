

const ts3 = require('../tsConnect');


/**
 * Get Clid From cluid
 *
 * @uid:       String    users.js Unique Identifier
 */
const getClidsFromUuid = (uuid) => {

    if ((_.isString(uuid))) {

        let params = {
            cluid: uuid
        };

        return ts3.execute("clientgetids", params)
            .then(data => {
                return data;
            })
            .catch(err => {
                throw failedApiReply('getClidFromUid error:', err)
            });

    } else {
        throw failedApiReply('getClidFromUid: Invalid Input', 500)
    }
};




/**
 * Get dbid From cluid
 *
 * @uid:       String    users.js Unique Identifier
 */
const getCldbidFromUid = (uid) => {


    if ((_.isString(uid))) {

        let params = {
            cluid: uid
        };

        return ts3.execute("clientgetdbidfromuid", params)
            .then(data => {
                return data[0].cldbid;
            })
            .catch(err => {
                throw failedApiReply(err, 'setChannelGroupbyUid: Error Saving data on the database');
            })

    } else {
        console.log('getCldbidFromUid: Invalid Input');
    }
};


const isMemberOnTheServer = (uuid) => {

    //Check is uuid is connected to
    let filteredClients = [];

    return ts3.clientList()
        .then(clients => {

            let promiseArr = clients.map(client => {
                if (_.isEqual(client._propcache.connection_client_ip, clientIp)) {
                    return true;
                }
                return false;
            });

            //Resolves and Checks if there was any problem with executiong returns results.
            return Promise.all(promiseArr)
                .then(users => {
                    if (users.includes(true))
                        return ApiReply('Member is connected!', true);
                    else
                        return ApiReply('Member is not connected', false);

                    // do something after the loop finishes
                }).catch(err => {
                    throw failedApiReply(err, 'API isMemberOnTheServer: Error happened while checking if the user is connected to the server');
                })
        }).catch(err => {
            throw failedApiReply(err, 'API isMemberOnTheServer: Failed to get clientList');
        })
}


module.exports = {
    getClidsFromUuid,
    getCldbidFromUid,
    isMemberOnTheServer,
    serverView,
}
