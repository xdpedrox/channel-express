const ts3 = require('../tsConnect');
const Core = require('./Core');
const ServerGroup = require('./ServerGroup')
const User = require('./User')
const Channel = require('./Channel')

/**
 * Gives a user ChannelGroup Permission on all the subchannels.
 * @param {Number} sgid - ServerGroup ID
 * @param {Number} mainChannelId - Main id of the channel / spacer
 * @param {String} uuid  - TeamSpeak Client Unique ID
 */
const set = (sgid, mainChannelId, uuid) => {

  //Get the List of the SubChannels;
  return Channel.subChannelList(mainChannelId)
    .then(channels => {
      //Get cldbid of users.js (Required to set Channel Admin)
      return TsServer.getCldbidFromUid(uuid)
        .then(cldbid => {
          //Works Like a ForEach Loop but it's async
          let promiseArr = channels.map(channel => {
            //Set ChannelGroup to user
            return ts3.setClientChannelGroup(sgid, channel._propcache.cid, cldbid)
          });

          //Resolves and Checks if there was any problem with executiong returns results.
          return Promise.all(promiseArr)
            .then(resultsArray => {
              return resultsArray;
              // return ApiReply('Channel Group Assigned', resultsArray)
              // do something after the loop finishes
            }).catch(err => {
              throw err
              // failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
              // do something when any of the promises in array are rejected
            })
          //
        })
        .catch(err => {
          throw err
          // failedApiReply(err, 'setChannelGroupbyUid: Error getting cldbid of client.');
        })
    })
    .catch(err => {
      throw err
      // failedApiReply(err, 'setChannelGroupbyUid: Error Getting SubChannel List.');
    })
}



/**
 * Removes all Channel Groups from everyone in the team and then reassigns them.
 * @param {String} teamObjectID - TeamObjectID from the Database
 */
const reassign = (mainChannelId, teamMembers) => {

  //Remove all ChannelGroup users from the channels.
  return Channel.removeChannelGroups(mainChannelId)
    .then(() => {

      //Get the List of the SubChannels;
      return Channel.subChannelList(mainChannelId)
        .then(channels => {

          //Works Like a ForEach Loop but it's async
          let promiseArr1 = channels.map(channel => {
            // return the promise to array

            //Works Like a ForEach Loop but it's async
            let promiseArr2 = teamMembers.map(teamMember => {
              // return the promise to array


              if (teamMember.permissions == 1) {
                channelGroupId = config.groupSettings.channelAdmin;

              } else if (teamMember.permissions == 2) {
                channelGroupId = config.groupSettings.channelAdmin;


              } else if (teamMember.permissions == 3) {
                channelGroupId = config.groupSettings.channelMod;

              } else if (teamMember.permissions == 4) {
                channelGroupId = config.groupSettings.channelMember;

              } else {
                channelGroupId = config.groupSettings.guestChannelGroup;

              }

              return TsServer.getCldbidFromUid(teamMember.uuid)
                .then(cldbid => {
                  return ts3.setClientChannelGroup(channelGroupId, channel._propcache.cid, cldbid)
                    .then(() => {
                      return ApiReply('Channel Group Assigned', member.memberUuid)
                    })
                })
            })


            //2 FOR EACH
            //Resolves and Checks if there was any problem with executiong returns results.
            return Promise.all(promiseArr2)
              .then(resultsArray2 => {

                return resultsArray2;
              })
              .catch(err => {
                throw failedApiReply(err, 'reassignChannelGroups: Set Channel Group Failed.');
              })
            //

          });


          //1 FOR EACH
          //Resolves and Checks if there was any problem with executiong returns results.
          return Promise.all(promiseArr1)
            .then(resultsArray1 => {
              return resultsArray1;
            })
            .catch(err => {
              throw failedApiReply(err, 'reassignChannelGroups: Failed getting ClDbID.');
            })
          //

        })
        .catch(err => {
          throw failedApiReply(err, 'reassignChannelGroups: Failed getting subChannelList.');
        })
    })
    .catch(err => {
      throw failedApiReply(err, 'reassignChannelGroups: Failed clearing ChannelGroup.');
    })

}



module.exports = {
  set,
  reassign,
}
