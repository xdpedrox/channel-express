const ts3 = require('../tsConnect');
const Core = require('./Core');
const ChannelGroup = require('./ChannelGroup')
const ServerGroup = require('./ServerGroup')
const User = require('./User')



const config = require('../config/ts3config')




/**
 * Create channel
 *
 * @public          boolean     Choose between public of private channel
 * @name:           String      Client DB ID
 * @password        String      Channel Password
 * @topic           String      Channel Topic
 * @description     String      Channel Description
 * @subChannel      int         Sub Channel ID (Not Required)
 * @channelOrder    int         Order of the Channel (Not Required)
 */
const create = (public, name, password, topic, description, subChannel, channelOrder) => {


  subChannel = parseInt(subChannel);
  channelOrder = parseInt(channelOrder);

  if ((_.isString(name)) & (_.isBoolean(public))) {

    //Set the channel Plublic or Private

    let properties = { ...channel.public };

    if (!public) {
      properties = { ...channel.private }
    }



    //Add the data into the object
    if (!_.isEmpty(password)) {
      properties.channel_password = password
    }

    if (!_.isEmpty(description)) {
      properties.channel_description = description
    }

    if (!_.isEmpty(topic)) {
      properties.channel_topic = topic
    }

    if (_.isInteger(subChannel)) {
      properties.cpid = subChannel
    }

    if (_.isInteger(channelOrder)) {
      properties.channel_order = channelOrder
    }

    return ts3.channelCreate(name, properties);
  }
}


/**
* Edit channel
*
* @public          boolean     Choose between public of private channel
* @cid             int         Source Server Group ID
* @name            String      Client DB ID
* @password        String      Channel Password
* @topic           String      Channel Topic
* @description:    String      Channel Description
*/
const edit = (public, cid, name, password, topic, description) => {

  cid = parseInt(cid);

  if ((_.isBoolean(public)) & (_.isInteger(cid))) {

    //Set the channel Plublic or Private

    let properties = { ...channel.public };

    if (!public) {
      properties = { ...channel.private }
    }



    //Add the data into the object
    if (!_.isEmpty(name)) {
      properties.channel_name = name
    }

    if (!_.isEmpty(password)) {
      properties.channel_password = password
    }

    if (!_.isEmpty(description)) {
      properties.channel_description = description
    }

    if (!_.isEmpty(topic)) {
      properties.channel_topic = topic
    }

    //console.log(properties);
    return ts3.channelEdit(cid, properties)
      .catch(err => {

        if (parseInt(err.id) == 771) {
          delete properties.channel_name;

          return ts3.channelEdit(cid, properties)
            .catch(err => {
              console.log(err)
            })
        } else {
          console.log(err)
        }
      })
  }
};




/**
 * Creates a group of channels - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} topic - Topic of the channel
 * @param {String} description - Description of the channel
 * @param {Number} spacerNumber - Number for the spacer.
 * @param {Number} lastCid - cid of the last channel of that area.
 */
const createPack = (name, password, topic, description, spacerNumber, lastCid) => {


  //Create the mainChannel
  return create(false, name, '', topic, description, '', lastCid)
    .then(mainChannel => {

      //Create Sub Channels
      create(true, config.channelSettings.subChannelName + ' 1', password, topic, description, mainChannel._propcache.cid, '')
      create(true, config.channelSettings.subChannelName + ' 2', password, topic, description, mainChannel._propcache.cid, '');
      create(true, config.channelSettings.subChannelName + ' 3', password, topic, description, mainChannel._propcache.cid, '');
      create(true, config.channelSettings.awayChannelName, password, topic, description, mainChannel._propcache.cid, '');

      //Create Spacer Bar
      return create(false, '[rspacer' + spacerNumber + ']', '', '', '', '', mainChannel._propcache.cid)
        .then(spacerChannel => {

          //Create Spacer Bar
          return create(false, '[*spacer' + spacerNumber + ']' + config.channelSettings.spacerBar, '', '', '', '', spacerChannel._propcache.cid)
            .then(spacerBar => {

              //Create a object with all the Channels Ids that were just created
              let channelIds = {
                mainChannelId: mainChannel._propcache.cid,
                spacerEmptyId: spacerChannel._propcache.cid,
                spacerBarId: spacerBar._propcache.cid
              }
              //Return Data
              return channelIds

            })
            .catch(err => {
              throw err;
              // failedApiReply(err, 'createGroupOfChannels: Failed to create Spacer Bar');
            })
        })
        .catch(err => {
          throw err;
          // failedApiReply(err, 'createGroupOfChannels: Failed to create Blank Spacer');
        })
    })
    .catch(err => {
      throw err;
      // throw failedApiReply(err, 'createGroupOfChannels: Failed to Main Channel/SubChannels');
    })
}

/**
 * Changes the name of the Team
 * @param {String} teamObjectID - Member objectID from the Database.
 * @param {String} name - New name for the team.
 */
const changeName = (channelName, name, mainChannelId, serverGroupId) => {

  let dt = new Date();

  //making the name

  //Edit the claimed channel
  return Core.edit(true, mainChannelId, channelName, '', 'topic', 'description')
    .then(() => {
      if (serverGroupId !== null) {
        return ts3.serverGroupRename(serverGroupId, name)
          .then(() => {
            return 'Success'
          })
          .catch(err => {
            throw failedApiReply(err, 'changeTeamName: Error Changing ServerGroup Name');
          })
      }
      return ApiReply('Name Changed Sucessfully', team)
    })

}

/**
* Sets SubChannels public
* @param int ChannelId
*/
const setSubChannelsPublic = (cid, password) => {

  cid = parseInt(cid);

  //Get the List of the SubChannels;
  return subChannelList(cid)
    .then(channels => {
      //If there aren;t any subchannels then throw error
      if (channels.length == 0) {
        throw failedApiReply('', 'setSubChannelsPublic: Channel doesn\'t exist');

      } else {

        //Works Like a ForEach Loop but it's async
        let promiseArr = channels.map((channel, i) => {
          //Making the Name
          subIndex = i + 1;

          if (!(subIndex == channels.length)) {
            subName = config.channelSettings.subChannelName + " " + subIndex;

          } else {
            subName = config.channelSettings.awayChannelName;
          }

          //Change the name of the channel
          return edit(true, channel._propcache.cid, subName, password, '', '')
            .then(() => {
              return ApiReply('Channel Edited', channel._propcache.cid)
            })
            .catch(err => {
              throw failedApiReply(err, 'setSubChannelsPublic: Failed to edit channel');
            })
        });

        //Resolves and Checks if there was any problem with executiong returns results.
        return Promise.all(promiseArr)
          .then(resultsArray => {
            return ApiReply('setSubChannelsPublic: Channel Edited', resultsArray)
            // do something after the loop finishes
          }).catch(err => {
            throw failedApiReply(err, 'setSubChannelsPublic: Error Editing.');
            // do something when any of the promises in array are rejected
          })
        //
      }
    })
    .catch(err => {
      throw failedApiReply(err, 'setSubChannelsPublic: Failed to get subChannelList');
    })

}


/**
 * Sets SubChannels private
 * @param int ChannelId
 */
const setSubChannelsPrivate = (cid) => {

  cid = parseInt(cid);

  //Get the List of the SubChannels;
  return subChannelList(cid)
    .then(channels => {
      if (channels.length == 0) {
        throw failedApiReply('', 'setSubChannelsPrivate: Channel doesn\'t exist');

      } else {

        //Works Like a ForEach Loop but it's async
        let promiseArr = channels.map((channel, i) => {
          //This is used to name the SubChannels
          subIndex = i + 1;
          //Making the Name
          if (!(subIndex == channels.length)) {
            subName = config.channelSettings.subChannelName + " " + subIndex;

          } else {
            subName = config.channelSettings.awayChannelName;
          }

          //Change the name of the channel
          return edit(false, channel._propcache.cid, subName, '', '', '')
            .then(() => {
              return ApiReply('Channel Edited', channel._propcache.cid)
            })
            .catch(err => {
              throw failedApiReply(err, 'setSubChannelsPrivate: Failed to edit channel');
            })
        });

        //Resolves and Checks if there was any problem with executiong returns results.
        return Promise.all(promiseArr)
          .then(resultsArray => {
            return ApiReply('setSubChannelsPrivate: Channel Edited', resultsArray)
            // do something after the loop finishes
          }).catch(err => {
            throw failedApiReply(err, 'setSubChannelsPrivate: Error Editing.');
            // do something when any of the promises in array are rejected
          })
        //
      }
    })
    .catch(err => {
      throw failedApiReply(err, 'setSubChannelsPrivate: Failed to get subChannelList');
    })

}


/**
 *  Removes all Channel Groups from everyone in the team
 * @param {Number} mainChannelId - Main Channel/Spacer ID
 */
const removeChannelGroups = (mainChannelId) => {


  cid = parseInt(mainChannelId);

  //Get the List of the SubChannels;
  return subChannelList(cid)
    .then(channels => {


      //Works Like a ForEach Loop but it's async
      let promiseArr1 = channels.map(channel => {

        //Works Like a ForEach Loop but it's async
        let promiseArr2 = config.ChannelGroupsAdmin.map(channelGroupId => {
          // return the promise to array

          //Get ChannelGroup object
          return ts3.getChannelGroupByID(channelGroupId)
            .then(channelGroup => {

              //Get the List of clients that are part of that ChannelGroup
              return channelGroup.clientList(channel._propcache.cid)
                .then(clients => {


                  //Works Like a ForEach Loop but it's async
                  let promiseArr3 = clients.map(client => {
                    // return the promise to array

                    return ts3.setClientChannelGroup(config.groupSettings.guestChannelGroup, client.cid, client.cldbid)
                      .then(() => {
                        return ApiReply('Channel Group Assigned', { cgid: channelGroupId })
                      })


                  })


                  //3 FOR EACH
                  //Resolves and Checks if there was any problem with executiong returns results.
                  return Promise.all(promiseArr3)
                    .then(resultsArray3 => {

                      return resultsArray3;
                    })
                    .catch(err => {
                      throw failedApiReply(err, 'channelRemoveChannelGroupClients: Error Setting ChannelGroup.');
                    })
                  //

                }).catch(err => {
                  if (err.id == 1281) {
                    return ApiReply('No Members in the group', { cgid: channelGroupId })
                  } else {
                    throw failedApiReply(err, 'reassignChannelGroups: Failed getting Client List.');
                  }
                })
            })
            .catch(err => {
              throw failedApiReply(err, 'reassignChannelGroups: Failed to getting channelGroup.');
            })
        })


        //2 FOR EACH
        //Resolves and Checks if there was any problem with executiong returns results.
        return Promise.all(promiseArr2)
          .then(resultsArray2 => {

            return resultsArray2;

          })
          .catch(err => {
            throw failedApiReply(err, 'reassignChannelGroups: Failed to get Channel Group by ID Or getting ClientList of group.');
          })
        //

      })


      //3 FOR EACH
      //Resolves and Checks if there was any problem with executiong returns results.
      return Promise.all(promiseArr1)
        .then(resultsArray1 => {

          return resultsArray1;
        })
        .catch(err => {
          throw failedApiReply(err, 'reassignChannelGroups: Failed to do Everything.');
        })
      //

    }).then(results => {
      return ApiReply('Sucess', results)

    }).catch(err => {
      throw failedApiReply(err, 'reassignChannelGroups: Failed to get SubChannel List.');
    })
}



/**
 * Get Array of SubChannels
 *
 * @cid:        int    Channel ID
 */
const subChannelList = (cid) => {

  cid = parseInt(cid);

  if ((_.isInteger(cid))) {
    //TODO: maybe add a way to see if the method as successful by returning true
    return ts3.channelList({ pid: cid })
      .then(channels => { return channels })
      .catch(err => {
        throw err;
        // failedApiReply('setChannelGroupbyUid: Error Saving data on the database', err)
      })

  } else {
    throw "Invalid Input";
    // failedApiReply('subChannelList: Invalid Input', 500)
  }
}


/**
 * Free's Up unused channels.
 */
const freeUpChanel = (areaName, channelOrder, mainChannelId, spacerNumber) => {

  //Make the Name of the Channel
  let channelNameFree = '[cspacer' + spacerNumber + ']' + areaName[0] + channelOrder + ' - ★ ' + config.channelSettings.freeSpacerName + ' ★'

  //Edit Channel to set the New Name
  edit(false, mainChannelId, channelNameFree, '', '', '')
    .catch(err => {
      throw failedApiReply(err, 'freeUpChanels: Failed to edit channel');
    })
    .then(() => {

      //Make all the SubChannels Private
      setSubChannelsPrivate(mainChannelId)
        .catch(err => {
          throw failedApiReply(err, 'freeUpChanels: Failed to setSubChannelsPrivate');
        })

    }).then(() => {


      removeChannelGroups(mainChannelId)
        .catch(err => {
          throw failedApiReply(err, 'freeUpChanels: Failed to channelRemoveChannelGroupClients');
        })


    }).then(() => {

      return 'success'
    }).catch(err => {
      throw failedApiReply(err, 'freeUpChanels: Failed to edit channel');
    })
}


module.exports = {
  create,
  edit,
  createPack,
  changeName,
  setSubChannelsPrivate,
  setSubChannelsPublic,
  removeChannelGroups,
  subChannelList,
  freeUpChanel
}
