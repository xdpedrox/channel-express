// Code Import

// const Team = require('./library/team')
const Core = require('./library/Core');
// const ChannelGroup = require('./library/ChannelGroup')
// const ServerGroup = require('./library/ServerGroup')
// const User = require('./library/User')
const Channel = require('./library/Channel')



// const Groups = require('./library/groups.js')
// const Server = require('./library/server.js')
// const tsServerGroups = require('./library/tsServerGroups.js')
const ts3 = require('./tsConnect')

module.exports = {
    Core,
    // Users,
    Channel,
    // ChannelGroup,
    // ServerGroup,
    // User,
    ts3,
}
