const TeamSpeak3 = require("ts3-nodejs-library")

// const ts3connect = 
//Creates a new Connection to a TeamSpeak Server
const ts3 = new TeamSpeak3({
    host: "127.0.0.1",
    queryport: 10011,
    serverport: 9987,
    username: "serveradmin",
    password: "SyhvJ9ra",
    nickname: "BotDoSwag2",
    antispam: false,
    antispamtimer: 350,
    keepalive: true
})

ts3.on("ready", () => {
    console.log('LIB - Connection Ready!')
})


//Error event gets fired when an Error during connecting or an Error during Processing of an Event happens
ts3.on("error", e => {
    console.log("LIB -Error - LIB ", e.message)
})

//Close event gets fired when the Connection to the TeamSpeak Server has been closed
//the e variable is not always set
ts3.on("close", e => {
    console.log("LIB - Connection has been closed!", e)
})


module.exports = ts3