const ts3 = require('../tsConnect');
const Core = require('./Core');
const ChannelGroup = require('./ChannelGroup')
const ServerGroup = require('./ServerGroup')
const User = require('./User')



const config = require('../config/ts3config')

/**
 * Creates a Channel a Channel - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Id
 * @param {Number} areaId - Id of the Area where the channel is going to be created.
 * @param {boolean} move - Move the Client to the channel
 */
const createChannels = (teamArea, name, password, topic, description, clid, move) => {

  //Generate the Name of the Channel
  let processedName = '[cspacer' + teamArea.nextSpacerNumber + ']' + teamArea.areaName[0] + teamArea.nextChannelNumber + ' - ★ ' + name + ' ★'
  //Create Channels
  return createGroupOfChannels(processedName, password, topic, description, teamArea.nextSpacerNumber, teamArea.lastChannelId)
    .then(channelIds => {
      console.log(channelIds)
      if (move) {
        moveToFirstChannel(channelIds.mainChannelId, clid)
          .catch(err => {
            console.log(err)
          })
      }
      return channelIds

    }).catch(err => {
      throw err
      // failedApiReply(err, 'createChannels: Failed to create Group of Channels');
    })

}


/**
 * Creates a group of channels - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} topic - Topic of the channel
 * @param {String} description - Description of the channel
 * @param {Number} spacerNumber - Number for the spacer.
 * @param {Number} lastCid - cid of the last channel of that area.
 */
const createGroupOfChannels = (name, password, topic, description, spacerNumber, lastCid) => {


  //Create the mainChannel
  return Core.create(false, name, '', topic, description, '', lastCid)
    .then(mainChannel => {

      //Create Sub Channels
      Core.create(true, config.channelSettings.subChannelName + ' 1', password, topic, description, mainChannel._propcache.cid, '')
      Core.create(true, config.channelSettings.subChannelName + ' 2', password, topic, description, mainChannel._propcache.cid, '');
      Core.create(true, config.channelSettings.subChannelName + ' 3', password, topic, description, mainChannel._propcache.cid, '');
      Core.create(true, config.channelSettings.awayChannelName, password, topic, description, mainChannel._propcache.cid, '');

      //Create Spacer Bar
      return Core.create(false, '[rspacer' + spacerNumber + ']', '', '', '', '', mainChannel._propcache.cid)
        .then(spacerChannel => {

          //Create Spacer Bar
          return Core.create(false, '[*spacer' + spacerNumber + ']' + config.channelSettings.spacerBar, '', '', '', '', spacerChannel._propcache.cid)
            .then(spacerBar => {

              //Create a object with all the Channels Ids that were just created
              let channelIds = {
                mainChannelId: mainChannel._propcache.cid,
                spacerEmptyId: spacerChannel._propcache.cid,
                spacerBarId: spacerBar._propcache.cid
              }
              //Return Data
              return channelIds

            })
            .catch(err => {
              throw err;
              // failedApiReply(err, 'createGroupOfChannels: Failed to create Spacer Bar');
            })
        })
        .catch(err => {
          throw err;
          // failedApiReply(err, 'createGroupOfChannels: Failed to create Blank Spacer');
        })
    })
    .catch(err => {
      throw err;
      // throw failedApiReply(err, 'createGroupOfChannels: Failed to Main Channel/SubChannels');
    })
}







const resetUserTeamPermissions = (mainChannelId, uuid, permission, serverGroupId) => {

  if (permission == 1) {
    channelGroupId = config.groupSettings.channelAdmin;

  } else if (permission == 2) {
    channelGroupId = config.groupSettings.channelAdmin;

  } else if (permission == 3) {
    channelGroupId = config.groupSettings.channelMod;

  } else if (permission == 4) {
    channelGroupId = config.groupSettings.channelMember;

  } else {
    return 5;

  }

  //Set Channel Groups
  return setChannelGroupbyUid(channelGroupId, mainChannelId, uuid)
    .then(data => {
      console.log(data)

      //If team has a servergroup, then add the user to it
      if (serverGroupId != null) {
        setServerGroupByUid(uuid, serverGroupId)
          .then(info => {
            console.log(info)
          })
          .catch(err => {
            if (err.id == 2561) {
              return 'users.js Already bellongs to this server group'
            }
            throw err
            'addUserToTeam: setClientServerGroupByUid failed';
            // throw failedApiReply(err, 'addUserToTeam: setClientServerGroupByUid failed');
          })
      }
      return data;
    })
    .catch(err => {
      throw err
      'addUserToTeam: Failed updating the database';
      // throw failedApiReply(err, 'addUserToTeam: Failed updating the database');
    })
}

const removeUserFromTeam = () => {




}


module.exports = {
  // Channel
  // createChannels,
  // moveUserToFirstChannel,
  // setChannelGroupbyUid,
  // setServerGroupByUid,
  // resetUserTeamPermissions
}
