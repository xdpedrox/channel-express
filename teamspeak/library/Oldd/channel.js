const ts3 = require('../tsConnect');
const tsCore = require('./tsCore');

const config = require('../config/ts3config')

/**
 * Creates a Channel a Channel - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Id
 * @param {Number} areaId - Id of the Area where the channel is going to be created.
 * @param {boolean} move - Move the Client to the channel
 */
const createChannels = (teamArea, name, password, topic, description, clid, move) => {

  //Generate the Name of the Channel
  let processedName = '[cspacer' + teamArea.nextSpacerNumber + ']' + teamArea.areaName[0] + teamArea.nextChannelNumber + ' - ★ ' + name + ' ★'
  //Create Channels
  return createGroupOfChannels(processedName, password, topic, description, teamArea.nextSpacerNumber, teamArea.lastChannelId)
    .then(channelIds => {
      console.log(channelIds)
      if (move) {
        moveToFirstChannel(channelIds.mainChannelId, clid)
        .catch(err => {
          console.log(err)
        })
      }
      return channelIds

    }).catch(err => {
      throw err
      // failedApiReply(err, 'createChannels: Failed to create Group of Channels');
    })

}



/**
 * Creates a group of channels - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} topic - Topic of the channel
 * @param {String} description - Description of the channel
 * @param {Number} spacerNumber - Number for the spacer.
 * @param {Number} lastCid - cid of the last channel of that area.
 */
const createGroupOfChannels = (name, password, topic, description, spacerNumber, lastCid) => {


  //Create the mainChannel
  return tsCore.create(false, name, '', topic, description, '', lastCid)
    .then(mainChannel => {

      //Create Sub Channels
      tsCore.create(true, config.channelSettings.subChannelName + ' 1', password, topic, description, mainChannel._propcache.cid, '')
      tsCore.create(true, config.channelSettings.subChannelName + ' 2', password, topic, description, mainChannel._propcache.cid, '');
      tsCore.create(true, config.channelSettings.subChannelName + ' 3', password, topic, description, mainChannel._propcache.cid, '');
      tsCore.create(true, config.channelSettings.awayChannelName, password, topic, description, mainChannel._propcache.cid, '');

      //Create Spacer Bar
      return tsCore.create(false, '[rspacer' + spacerNumber + ']', '', '', '', '', mainChannel._propcache.cid)
        .then(spacerChannel => {

          //Create Spacer Bar
          return tsCore.create(false, '[*spacer' + spacerNumber + ']' + config.channelSettings.spacerBar, '', '', '', '', spacerChannel._propcache.cid)
            .then(spacerBar => {

              //Create a object with all the Channels Ids that were just created
              let channelIds = {
                mainChannelId: mainChannel._propcache.cid,
                spacerEmptyId: spacerChannel._propcache.cid,
                spacerBarId: spacerBar._propcache.cid
              }
              //Return Data
              return channelIds

            })
            .catch(err => {
              throw err;
              // failedApiReply(err, 'createGroupOfChannels: Failed to create Spacer Bar');
            })
        })
        .catch(err => {
          throw err;
          // failedApiReply(err, 'createGroupOfChannels: Failed to create Blank Spacer');
        })
    })
    .catch(err => {
      throw err;
      // throw failedApiReply(err, 'createGroupOfChannels: Failed to Main Channel/SubChannels');
    })
}


/**
 * Gets the Top free Channel on a Specific areaID
 * @param {Number} areaId
 */
const getTopFreeChannel = (areaId) => {

  return Teams.findOne({
      status: 'free',
      areaId: areaId
    })
    .sort('+channelOrder')
    .then((team) => {
      return (team);
    })
    .catch(err => {
      throw failedApiReply(err, 'getTopFreeChannel: Failed to get db');
    })


}


/**
 * Free's Up unused channels.
 */
const freeUpChanels = () => {

  return Teams.find()
    .then((teams) => {

      //Works Like a ForEach Loop but it's async
      let promiseArr = teams.map(team => {
        // return the promise to array

        let timeDiference = new Date() - team.lastUsed;


        if ((team.status == 'clean') || ((team.status == 'OK') && timeDiference > 14400 * 60000)) {
          //    if ((team.status == 'clean')) {
          //Get the First Letter of the Area Name
          return gameArea.findById(team.areaId)
            .then((area) => {
              if (!area) {
                console.log('Area Not Found!');
              }

              //Make the Name of the Channel
              let channelNameFree = '[cspacer' + team.spacerNumber + ']' + area.areaName[0] + team.channelOrder + ' - ★ ' + config.channelSettings.freeSpacerName + ' ★'

              //Edit Channel to set the New Name
              tsCore.edit(false, team.mainChannelId, channelNameFree, '', '', '')
                .catch(err => {
                  throw failedApiReply(err, 'freeUpChanels: Failed to edit channel');
                })


            }).then(() => {

              //Make all the SubChannels Private
              setSubChannelsPrivate(team.mainChannelId)
                .catch(err => {
                  throw failedApiReply(err, 'freeUpChanels: Failed to setSubChannelsPrivate');
                })

            }).then(() => {


              channelRemoveChannelGroupClients(team.mainChannelId)
                .catch(err => {
                  throw failedApiReply(err, 'freeUpChanels: Failed to channelRemoveChannelGroupClients');
                })


            }).then(() => {

              //Set channel free in the database.
              return Teams.findByIdAndUpdate(team._id, {
                $set: {
                  serverGroupId: null,
                  status: 'free',
                  ownerID: null,
                  members: []
                }

              }).then(() => {
                return ApiReply('freeUpChanels: Channel freed up', {
                  name: team.teamName,
                  _id: team._id
                })

              }).catch(err => {
                throw failedApiReply(err, 'freeUpChanels: Failed to find team db and Update');
              })
            })

        } else if (team.status == 'free') {
          return ApiReply('freeUpChanels: Channel is already free', {
            name: team.teamName,
            _id: team._id
          })
        } else {
          return ApiReply('freeUpChanels: Channel is still in use', {
            name: team.teamName,
            _id: team._id
          })
        }
      });


      //Resolves and Checks if there was any problem with executiong returns results.
      return Promise.all(promiseArr)
        .then(resultsArray => {
          return ApiReply('freeUpChanels: Sucess', resultsArray)
          // do something after the loop finishes
        }).catch(err => {
          throw failedApiReply(err, 'freeUpChanels: For Each Failed.');
          // do something when any of the promises in array are rejected
        })
      //

    }, (err) => {
      throw failedApiReply(err, 'freeUpChanels: Failed to find team');
    })
    .catch(err => {
      throw failedApiReply(err, 'freeUpChanels: Failed to find team');
    })
}

////////////////////////////////////
//      Main/Exported Methods     //
////////////////////////////////////


/**
 * Moves client to the first channel of a team.
 * @param {Number} mainChannelId - CID of the Main Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 */
const moveToFirstChannel = (mainChannelId, clid) => {

  return tsCore.subChannelList(mainChannelId)
    .then(channels => {
      //Set Channel Group to all SubChannels
      return ts3.clientMove(clid, channels[0]._propcache.cid)
        .catch(err => {
          if (err.id == 770) {
            // console.log('exec')
            // console.log(err);
            return "users.js is already in the channel"
          } else{
            throw err;
          }
          // failedApiReply(err, 'moveToFirstChannel: Failed to Move Client');
        })
    })
    .catch(err => {
      // console.log('lol')
      throw err;
      // failedApiReply(err, 'moveToFirstChannel: Failed to get subChannelList');
    })
}


/**
 * Adds client to a ServerGroup
 * @param {String} ownerUuid - TeamSpeak users.js Unique Identifier
 * @param {Number} sgid - ServerGroup Id
 */
const setClientServerGroupByUid = (ownerUuid, sgid) => {

  //Get users.js DBID
  return tsCore.getCldbidFromUid(ownerUuid)
    .then(cldbid => {
      //Get list of SubChannels
      return ts3.serverGroupAddClient(cldbid, sgid)
        .then((data) => {
          return ApiReply('setClientServerGroupByUid: Server Group Assigned', data)
        })
        .catch(err => {
          throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
        })

    })
    .catch(err => {
      throw failedApiReply(err, 'setClientServerGroupByUid: Failed to get cldbid');
    })
}


/**
 * Swaps channel position with another channel.
 * @param {string} teamObjectID - Team objectID from the Database
 * @param {String} memberObjectID - Member objectID from the Database
 */
const moveChannel = (oldTeam, newTeamObjectID) => {

  let dt = new Date();

  //Getting New Team data from the Database
  return Teams.findById(newTeamObjectID)
    .then((newTeam) => {

      if (_.isEqual(newTeam.status, 'free')) {

        //TODO Got to add the new Variables in the DB to copy over those things as well.

        //Update New Team Database with location info of old team
        Teams.findByIdAndUpdate(
            newTeamObjectID, {
              $set: {
                channelOrder: oldTeam.channelOrder,
                spacerNumber: oldTeam.spacerNumber,
                mainChannelId: oldTeam.mainChannelId,
                spacerEmptyId: oldTeam.spacerEmptyId,
                spacerBarId: oldTeam.spacerBarId,
                areaId: oldTeam.areaId,
                status: 'clean'
              }
            })
          .catch(err => {
            throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
          })


        //Update Old Team Database with location info of New team
        return Teams.findByIdAndUpdate(
            oldTeam._id, {
              $set: {
                channelOrder: newTeam.channelOrder,
                spacerNumber: newTeam.spacerNumber,
                mainChannelId: newTeam.mainChannelId,
                spacerEmptyId: newTeam.spacerEmptyId,
                spacerBarId: newTeam.spacerBarId,
                areaId: newTeam.areaId,
                nextMove: new Date(dt.getTime() + 720 * 60000) //720 Minutes
              }
            }, {
              new: true
            })
          .then(team => {

            //Claim Channels on TeamSpeak
            return claimChannels(team.teamName, '', team.ownerID, team._id, true)
              .then(() => {

                //Free Up Old Channel
                return freeUpChanels()
                  .then(() => {

                    //Giving Channel Permission to Users
                    return reassignChannelGroups(team._id)
                      .then(info => {
                        //Return Success!
                        return ApiReply('Channel Group Assigned', team)
                      })
                      .catch(err => {
                        throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
                      })

                  })
                  .catch(err => {
                    throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
                  })

              })
              .catch(err => {
                throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
              })
          })
          .catch(err => {
            throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
          })

      } else {
        throw failedApiReply('err', 'moveChannel: Team Selected is not free.');
      }
    })
    .catch(err => {
      throw failedApiReply(err, 'addUserToTeam: Failed fetching database members.');
    })

}


/**
 * Claims a Channel - Used by another function not to use by itself
 * @param {String} name - Name of the Channel
 * @param {String} password - Password of the Channel
 * @param {String} ownerUuid - TeamSpeak users.js Unique Id
 * @param {Number} teamObjectID -Database Object id of the Team to claim
 * @param {boolean} move - Move the Client to the channel
 */
const claimChannels = (name, password, ownerUuid, teamObjectID, move) => {

  return Teams.findById(teamObjectID).then((team) => {

    gameArea.findById(team.areaId)
      .then((area) => {
        if (!area) {
          throw failedApiReply('err', 'claimChannels: No Area Claiming');
        }

        //making the name
        let processedName = '[cspacer' + team.spacerNumber + ']' + area.areaName[0] + team.channelOrder + ' - ★ ' + name + ' ★';

        //Edit the claimed channel
        return tsCore.edit(true, team.mainChannelId, processedName, '', 'topic', 'description')
          .then(() => {


            //Make subchannels Public
            setSubChannelsPublic(team.mainChannelId, password)
              .catch(err => {
                throw failedApiReply(err, 'claimChannels: Failed to edit channel');
              })


            //Move client to channel
            if (move) {
              moveToFirstChannel(team.mainChannelId, ownerUuid)
            }

            //Update Database Records
            return Teams.findByIdAndUpdate(
                team._id, {
                  $set: {
                    teamName: name,
                    status: 'OK',
                    ownerID: ownerUuid,
                  }
                })

              .then((teamClaimed) => {
                if (!teamClaimed) {
                  throw failedApiReply(err, 'claimChannels: No db');
                }

                return ApiReply('createChannels: Channels Sucessfully Created', teamClaimed)

              }).catch(err => {
                throw failedApiReply(err, 'claimChannels: Failed to edit Database');
              })

          }).catch(err => {
            throw failedApiReply(err, 'claimChannels: Failed to edit channel!');
          })


      })

  }).catch(err => {
    throw failedApiReply(err, 'createChannels: Failed to find gameArea');
  })
}


const channelView = (cid) => {
  return serverView()
    .then(view => {
      // console.log(view);
      return view.filter(channel => channel.pid == cid);
    }).catch(err => {
      throw failedApiReply(err, 'API channelView: Failed to get channels');
    })
}



/**
 * Removes all Channel Groups from everyone in the team and then reassigns them.
 * @param {String} teamObjectID - TeamObjectID from the Database
 */
const reassignChannelGroups = (teamObjectID) => {

  //Fetch Team from DB
  return Teams.findById(teamObjectID)
    .then(team => {

      //Remove all ChannelGroup users from the channels.
      return channelRemoveChannelGroupClients(team.mainChannelId)
        .then(() => {

          //Get the List of the SubChannels;
          return tsCore.subChannelList(team.mainChannelId)
            .then(channels => {

              //Works Like a ForEach Loop but it's async
              let promiseArr1 = channels.map(channel => {
                // return the promise to array

                //Works Like a ForEach Loop but it's async
                let promiseArr2 = team.members.map(member => {
                  // return the promise to array


                  if (member.permissions == 1) {
                    channelGroupId = config.groupSettings.channelAdmin;

                  } else if (member.permissions == 2) {
                    channelGroupId = config.groupSettings.channelAdmin;


                  } else if (member.permissions == 3) {
                    channelGroupId = config.groupSettings.channelMod;

                  } else if (member.permissions == 4) {
                    channelGroupId = config.groupSettings.channelMember;

                  } else {
                    channelGroupId = config.groupSettings.guestChannelGroup;

                  }

                  return tsCore.getCldbidFromUid(member.memberUuid)
                    .then(cldbid => {
                      return ts3.setClientChannelGroup(channelGroupId, channel._propcache.cid, cldbid)
                        .then(() => {
                          return ApiReply('Channel Group Assigned', member.memberUuid)
                        })
                    })
                })


                //2 FOR EACH
                //Resolves and Checks if there was any problem with executiong returns results.
                return Promise.all(promiseArr2)
                  .then(resultsArray2 => {

                    return resultsArray2;
                  })
                  .catch(err => {
                    throw failedApiReply(err, 'reassignChannelGroups: Set Channel Group Failed.');
                  })
                //

              });


              //1 FOR EACH
              //Resolves and Checks if there was any problem with executiong returns results.
              return Promise.all(promiseArr1)
                .then(resultsArray1 => {
                  return resultsArray1;
                })
                .catch(err => {
                  throw failedApiReply(err, 'reassignChannelGroups: Failed getting ClDbID.');
                })
              //

            })
            .catch(err => {
              throw failedApiReply(err, 'reassignChannelGroups: Failed getting subChannelList.');
            })
        })
        .catch(err => {
          throw failedApiReply(err, 'reassignChannelGroups: Failed clearing ChannelGroup.');
        })
    })
    .catch(err => {
      throw failedApiReply(err, 'reassignChannelGroups: Failed getting Team from the database.');
    })
}


/**
 *  Removes all Channel Groups from everyone in the team
 * @param {Number} mainChannelId - Main Channel/Spacer ID
 */
const channelRemoveChannelGroupClients = (mainChannelId) => {


  cid = parseInt(mainChannelId);

  //Get the List of the SubChannels;
  return tsCore.subChannelList(cid)
    .then(channels => {


      //Works Like a ForEach Loop but it's async
      let promiseArr1 = channels.map(channel => {

        //Works Like a ForEach Loop but it's async
        let promiseArr2 = config.ChannelGroupsAdmin.map(channelGroupId => {
          // return the promise to array

          //Get ChannelGroup object
          return ts3.getChannelGroupByID(channelGroupId)
            .then(channelGroup => {

              //Get the List of clients that are part of that ChannelGroup
              return channelGroup.clientList(channel._propcache.cid)
                .then(clients => {


                  //Works Like a ForEach Loop but it's async
                  let promiseArr3 = clients.map(client => {
                    // return the promise to array

                    return ts3.setClientChannelGroup(config.groupSettings.guestChannelGroup, client.cid, client.cldbid)
                      .then(() => {
                        return ApiReply('Channel Group Assigned', {
                          cgid: channelGroupId
                        })
                      })


                  })


                  //3 FOR EACH
                  //Resolves and Checks if there was any problem with executiong returns results.
                  return Promise.all(promiseArr3)
                    .then(resultsArray3 => {

                      return resultsArray3;
                    })
                    .catch(err => {
                      throw failedApiReply(err, 'channelRemoveChannelGroupClients: Error Setting ChannelGroup.');
                    })
                  //

                }).catch(err => {
                  if (err.id == 1281) {
                    return ApiReply('No Members in the group', {
                      cgid: channelGroupId
                    })
                  } else {
                    throw failedApiReply(err, 'reassignChannelGroups: Failed getting Client List.');
                  }
                })
            })
            .catch(err => {
              throw failedApiReply(err, 'reassignChannelGroups: Failed to getting channelGroup.');
            })
        })


        //2 FOR EACH
        //Resolves and Checks if there was any problem with executiong returns results.
        return Promise.all(promiseArr2)
          .then(resultsArray2 => {

            return resultsArray2;

          })
          .catch(err => {
            throw failedApiReply(err, 'reassignChannelGroups: Failed to get Channel Group by ID Or getting ClientList of group.');
          })
        //

      })


      //3 FOR EACH
      //Resolves and Checks if there was any problem with executiong returns results.
      return Promise.all(promiseArr1)
        .then(resultsArray1 => {

          return resultsArray1;
        })
        .catch(err => {
          throw failedApiReply(err, 'reassignChannelGroups: Failed to do Everything.');
        })
      //

    }).then(results => {
      return ApiReply('Sucess', results)

    }).catch(err => {
      throw failedApiReply(err, 'reassignChannelGroups: Failed to get SubChannel List.');
    })
}



/**
 * Gets and array of the channels that are free.
 */
const getFreeTeams = () => {
  //Find all the Teams that are Free
  return Teams.find({
      status: 'free'
    }).then((teams) => {
      return ApiReply('setSubChannelsPublic: Channel Edited', teams)
    })
    .catch(err => {
      throw failedApiReply(err, 'setClientChannelGroupUid: Failed to get cldbid');
    })
}



/** Needs work
 * Crawls the TeamSpeak every few minutes and stores when the channels were last used.
 * Used for channel deletion.
 */
const crawlerChannels = () => {

  return ts3.channelList()
    .then(channels => {
      //console.log(channels)

      return Teams.find({
          status: 'OK'
        })
        .then(teams => {
          //console.log(teams)

          //Works Like a ForEach Loop but it's async
          let promiseArr = teams.map(team => {
            // return the promise to array

            return team.mainChannelId;
          });

          //Resolves and Checks if there was any problem with executiong returns results.
          return Promise.all(promiseArr)
            .then(mainChannelsIDs => {
              return mainChannelsIDs;
              // do something after the loop finishes
            }).catch(err => {
              throw failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
              // do something when any of the promises in array are rejected
            })
          //
        })


        .then(mainChannelsIDs => {

          edited = [];


          //Works Like a ForEach Loop but it's async
          let promiseArr2 = channels.map(channel => {
            //console.log(mainChannelsIDs)

            //If the channel parent ID is in the mainChannelsIDs array then do this
            if (mainChannelsIDs.includes(channel._propcache.pid)) {

              //if the channel has users inside and the team hasn't been edited yet.
              if (channel._propcache.total_clients > 0 & !(edited.includes(channel._propcache.pid))) {
                //Add the channel to the list of Edited channels
                edited.push(channel._propcache.pid)

                //Update lastused on the DB
                return Teams.findOneAndUpdate({
                    mainChannelId: channel._propcache.pid
                  }, {
                    $set: {
                      lastUsed: Date.now(),
                    }
                  })
                  .then(info => {
                    return ApiReply("Channel Updated.", channel._propcache.cid)
                  })
                //Save to the Database update timeStamp with the current date.

              } else {
                return ApiReply("Channel has already been edited or doesn't have anyone inside.", channel._propcache.cid)
              }
            } else {
              return ApiReply("Channel doesn't belong to any team / is a spacer!", channel._propcache.cid)
            }
          });


          //Resolves and Checks if there was any problem with executiong returns results.
          return Promise.all(promiseArr2)
            .then(resultsArray => {
              return ApiReply('Channel Group Assigned', resultsArray)
              // do something after the loop finishes
            }).catch(err => {
              throw failedApiReply(err, 'setChannelGroupbyUid: Error Setting ChannelGroup.');
              // do something when any of the promises in array are rejected
            })
          //

        })

        .catch(err => {
          throw failedApiReply(err, 'createTeam: Failed to fetch Team DB');
        })

    })
    .catch(err => {
      throw failedApiReply(err, 'createTeam: Failed to fetch Team DB');
    })

}



module.exports = {
  // Channel
  createChannels,
  freeUpChanels,
  moveChannel,
  moveToFirstChannel,
  claimChannels,
  channelView,
  createGroupOfChannels,
  getTopFreeChannel,
  reassignChannelGroups,
  channelRemoveChannelGroupClients,
  getFreeTeams,
  crawlerChannels,
}
